import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import TaskApp from './TaskApp';

const App = () => (
  <Provider store={store}>
    <div className="app">
      <header />
      <div>
        <TaskApp />
      </div>
    </div>
  </Provider>
);

render(<App />, document.getElementById('app'));
