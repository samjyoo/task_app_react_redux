import React from 'react';
import PropTypes from 'prop-types';

const TaskItem = props => (
  <div className="row">
    <div className={`task-item ${props.onFocus ? 'focus' : ''}`}>
      <p className="item-text">{props.task}</p>
      <i className="fa fa-pencil edit" aria-hidden="true" onClick={() => props.onEdit(props.task)} />
      <i className="fa fa-trash-o delete" aria-hidden="true" onClick={() => props.onDelete(props.task)} />
    </div>
  </div>
);

TaskItem.defaultProps = {
  task: '',
  onFocus: false,
  onDelete: Function,
  onEdit: Function
};

TaskItem.propTypes = {
  task: PropTypes.string,
  onFocus: PropTypes.bool,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func
};

export default TaskItem;
