import axios from 'axios';
import {
  SET_NEW_TASK,
  ADD_API_DATA,
  CLICK_ADD_MODAL,
  HIDE_MSG,
  SET_SUCCESS_MSG,
  SET_ERROR_MSG,
  ADD_TASK,
  DELETE_TASK,
  SET_SAVE_DEFAULT,
  EDIT_TASK,
  SET_EDIT,
  SAVE_TASKS
} from './actions';

export function setNewTask(task) {
  return { type: SET_NEW_TASK, payload: task };
}

export function addTask() {
  return { type: ADD_TASK, payload: '' };
}

export function addApiData(data) {
  return { type: ADD_API_DATA, payload: data };
}

export function clickAddModal(bool) {
  return { type: CLICK_ADD_MODAL, payload: bool };
}

export function hideMsg() {
  return { type: HIDE_MSG, payload: false };
}

export function setErrorMsg() {
  return { type: SET_ERROR_MSG, payload: true };
}

export function setSuccessMsg() {
  return { type: SET_SUCCESS_MSG, payload: true };
}

export function deleteTask(task) {
  return { type: DELETE_TASK, payload: task };
}

export function editTask() {
  return { type: EDIT_TASK, payload: '' };
}

export function setEdit(task) {
  return { type: SET_EDIT, payload: task };
}

export function setSaveDefault() {
  return { type: SET_SAVE_DEFAULT, payload: false };
}

export function saveTasks() {
  return { type: SAVE_TASKS, payload: '' };
}

export function getApiData() {
  return dispatch => {
    axios({
      method: 'GET',
      url: `http://cfassignment.herokuapp.com/<samuelyoo>/tasks`,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        'Access-Control-Allow-Methods': 'GET',
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        if (response.data.tasks != null && response.data.tasks.length > 0) {
          dispatch(addApiData(response.data.tasks));
        }
      })
      .catch(() => {
        dispatch(setErrorMsg());
      });
  };
}

export function postApiData(add, tasks) {
  return dispatch => {
    if (add) {
      axios
        .post(`http://cfassignment.herokuapp.com/<samuelyoo>/tasks`, {
          tasks
        })
        .then(() => {
          dispatch(saveTasks());
        })
        .catch(() => {
          dispatch(setErrorMsg());
        });
    }
  };
}
