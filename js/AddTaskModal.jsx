import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { clickAddModal, setNewTask, addTask, editTask } from './actionCreators';

const AddTaskModal = props => (
  <div>
    {props.addModal &&
      <div className="add-task modal">
        <i className="fa fa-times-circle" aria-hidden="true" onClick={props.clickAddModal} />
        <textarea
          name=""
          id=""
          placeholder={`${props.edit.on ? 'Edit Task' : 'Add New Task'}`}
          onChange={props.handleNewTermChange}
          value={props.newTask}
        />
        {!props.edit.on && <button className="btn confirm add" onClick={props.addTask}>Add Task</button>}
        {props.edit.on && <button className="btn confirm edit" onClick={props.editTask}>Edit Task</button>}
      </div>}
  </div>
);

AddTaskModal.defaultProps = {
  newTask: '',
  addModal: false,
  edit: { on: false, index: null },
  handleNewTermChange: Function,
  clickAddModal: Function,
  addTask: Function,
  editTask: Function
};

AddTaskModal.propTypes = {
  newTask: PropTypes.string,
  addModal: PropTypes.bool,
  edit: PropTypes.objectOf(PropTypes.any),
  handleNewTermChange: PropTypes.func,
  clickAddModal: PropTypes.func,
  addTask: PropTypes.func,
  editTask: PropTypes.func
};

const mapStateToProps = state => ({
  newTask: state.newTask,
  addModal: state.addModal,
  edit: state.edit
});

const mapDispatchToProps = dispatch => ({
  handleNewTermChange(event) {
    dispatch(setNewTask(event.target.value));
  },

  clickAddModal() {
    dispatch(clickAddModal());
  },

  addTask() {
    dispatch(addTask());
  },

  editTask() {
    dispatch(editTask());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTaskModal);
