import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Body from './Body';
import Modals from './Modals';
import { getApiData } from './actionCreators';

class TaskApp extends Component {
  componentDidMount() {
    this.props.getApiData();
  }

  render() {
    return (
      <div className="container">
        <div className={`${this.props.addModal ? '' : 'hide'} overlay`} />
        <Modals />
        <Body {...this.props} />
      </div>
    );
  }
}

TaskApp.defaultProps = {
  addModal: false,
  getApiData: Function
};

TaskApp.propTypes = {
  addModal: PropTypes.bool,
  getApiData: PropTypes.func
};

const mapStateToProps = state => ({
  tasks: state.tasks,
  addModal: state.addModal,
  add: state.add
});

const mapDispatchToProps = dispatch => ({
  getApiData(data) {
    dispatch(getApiData(data));
  }
});

export const Unwrapped = TaskApp;
export default connect(mapStateToProps, mapDispatchToProps)(TaskApp);
