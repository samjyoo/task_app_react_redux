import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TaskList from './TaskList';
import { clickAddModal, postApiData } from './actionCreators';

const Body = props => (
  <div>
    <section className="title">
      <div className="row">
        <h2>Tasks</h2>
        <button className="add btn" onClick={props.clickAddModal}>Add Task</button>
        <button className={`save btn ${props.add ? '' : 'shade'}`} onClick={props.postApiData}>Save</button>
      </div>
    </section>
    <TaskList />
  </div>
);

Body.defaultProps = {
  add: false,
  clickAddModal: Function,
  postApiData: Function
};

Body.propTypes = {
  add: PropTypes.bool,
  clickAddModal: PropTypes.func,
  postApiData: PropTypes.func
};

const mapStateToProps = state => ({
  addModal: state.addModal,
  add: state.add
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  clickAddModal() {
    dispatch(clickAddModal());
  },
  postApiData() {
    dispatch(postApiData(ownProps.add, ownProps.tasks));
  }
});

export const Unwrapped = Body;
export default connect(mapStateToProps, mapDispatchToProps)(Body);
