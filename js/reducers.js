import {
  SET_NEW_TASK,
  ADD_API_DATA,
  CLICK_ADD_MODAL,
  HIDE_MSG,
  SET_ERROR_MSG,
  SET_SUCCESS_MSG,
  ADD_TASK,
  DELETE_TASK,
  SET_SAVE_DEFAULT,
  SET_EDIT,
  EDIT_TASK,
  SAVE_TASKS
} from './actions';

const DEFAULT_STATE = {
  newTask: '',
  tasks: [],
  addModal: false,
  successMsg: false,
  errorMsg: false,
  add: false,
  edit: { on: false, index: null }
};

const setNewTask = (state, action) => Object.assign({}, state, { newTask: action.payload });
const addApiData = (state, action) => Object.assign({}, state, { tasks: action.payload });
const clickAddModel = state => Object.assign({}, state, { addModal: !state.addModal });
const hideMsg = (state, action) => Object.assign({}, state, { successMsg: action.payload, errorMsg: action.payload });
const setSuccessMsg = (state, action) => Object.assign({}, state, { successMsg: action.payload });
const setErrorMsg = (state, action) => Object.assign({}, state, { errorMsg: action.payload });
const addTask = state => {
  const newTaskArray = state.tasks.slice() || [];
  newTaskArray.push(state.newTask);
  return Object.assign({}, state, { tasks: newTaskArray, add: true, addModal: false, newTask: '' });
};
const deleteTask = (state, action) => {
  const newTaskArray = state.tasks.slice();
  newTaskArray.splice(state.tasks.indexOf(action.payload), 1);
  return Object.assign({}, state, { tasks: newTaskArray, add: true });
};
const setSaveDefault = (state, action) => Object.assign({}, state, { add: action.payload });
const setEdit = (state, action) => {
  const index = state.tasks.indexOf(action.payload);
  return Object.assign({}, state, { edit: { on: true, index }, addModal: true });
};
const editTask = state => {
  const newTaskArray = state.tasks.slice();
  newTaskArray[state.edit.index] = state.newTask;
  return Object.assign({}, state, {
    tasks: newTaskArray,
    add: true,
    addModal: false,
    newTask: '',
    edit: { on: false, index: state.edit.index }
  });
};
const saveTasks = state => Object.assign({}, state, {successMsg: true,edit: { on: false, index: null },add: false});

const rootReducer = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case SET_NEW_TASK:
      return setNewTask(state, action);
    case ADD_API_DATA:
      return addApiData(state, action);
    case ADD_TASK:
      return addTask(state);
    case CLICK_ADD_MODAL:
      return clickAddModel(state, action);
    case HIDE_MSG:
      return hideMsg(state, action);
    case SET_ERROR_MSG:
      return setErrorMsg(state, action);
    case SET_SUCCESS_MSG:
      return setSuccessMsg(state, action);
    case DELETE_TASK:
      return deleteTask(state, action);
    case SET_SAVE_DEFAULT:
      return setSaveDefault(state, action);
    case SET_EDIT:
      return setEdit(state, action);
    case EDIT_TASK:
      return editTask(state, action);
    case SAVE_TASKS:
      return saveTasks(state);
    default:
      return state;
  }
};

export default rootReducer;
