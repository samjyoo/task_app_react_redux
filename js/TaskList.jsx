import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { deleteTask, setEdit } from './actionCreators';
import TaskItem from './TaskItem';

const TaskList = props => {
  let taskList = props.tasks;

  if (taskList.length <= 0) {
    taskList = <div className="row">no results</div>;
  } else {
    /* eslint-disable */
    taskList = taskList.map((task, i) => {
      let focus;
      if (i === props.edit.index) {
        focus = true;
      } else {
        focus = false;
      }
      return <TaskItem key={i} task={task} onDelete={props.deleteTask} onEdit={props.setEdit} onFocus={focus} />;
    });
  }

  return (
    <section className="task-list">
      {taskList}
    </section>
  );
};

TaskList.defaultProps = {
  tasks: [],
  deleteTask: Function
};

TaskList.propTypes = {
  tasks: PropTypes.arrayOf(PropTypes.string),
  deleteTask: PropTypes.func
};

const mapStateToProps = state => ({
  tasks: state.tasks,
  edit: state.edit
});

const mapDispatchToProps = dispatch => ({
  deleteTask(task) {
    dispatch(deleteTask(task));
  },
  setEdit(task) {
    dispatch(setEdit(task));
  }
});

export const Unwrapped = TaskList;
export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
