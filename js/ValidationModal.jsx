import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { hideMsg } from './actionCreators';

const ValidationModal = props => (
  <div>
    {props.successMsg &&
      <div className={`success-message modal`}>
        Task List has been successfully saved!
        <i className="fa fa-times-circle" aria-hidden="true" onClick={props.hideMsg} />
      </div>}

    {props.errorMsg &&
      <div className={`error-message modal`}>
        There was an error. Please try again.
        <i className="fa fa-times-circle" aria-hidden="true" onClick={props.hideMsg} />
      </div>}
  </div>
);

ValidationModal.defaultProps = {
  successMsg: false,
  errorMsg: false,
  hideMsg: Function
};

ValidationModal.propTypes = {
  successMsg: PropTypes.bool,
  errorMsg: PropTypes.bool,
  hideMsg: PropTypes.func
};

const mapStateToProps = state => ({
  successMsg: state.successMsg,
  errorMsg: state.errorMsg
});

const mapDispatchToProps = dispatch => ({
  hideMsg() {
    dispatch(hideMsg());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ValidationModal);
