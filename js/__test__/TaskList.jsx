import React from 'react';
import { shallow } from 'enzyme';
import TaskList, { Unwrapped as UnwrappedTaskList } from '../TaskList';
import TaskItem from '../TaskItem';

const tasks = ['Buy Milk', 'Call Bob', 'Go To Gym', 'Read Book', 'Water Plants'];

test('TaskList renders correctly', () => {
  const component = shallow(<UnwrappedTaskList tasks={['Buy Milk', 'Meet Bob']} />);
  expect(component).toMatchSnapshot();
});

test('Task list should render correct amount of items', () => {
  const component = shallow(<UnwrappedTaskList tasks={tasks} />);
  expect(component.find(TaskItem).length).toEqual(tasks.length);
});
