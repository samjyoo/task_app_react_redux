import React from 'react';
import { render } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../store';
import {
  clickAddModal,
  setNewTask,
  addTask,
  deleteTask,
  setErrorMsg,
  setSuccessMsg,
  setEdit,
  editTask
} from '../actionCreators';
import TaskApp from '../TaskApp';

test('Task App renders correctly', () => {
  const component = render(
    <Provider store={store}>
      <TaskApp />
    </Provider>
  );
  expect(component).toMatchSnapshot();
});

test('modal appears after clicking add task button', () => {
  store.dispatch(clickAddModal());

  const component = render(
    <Provider store={store}>
      <TaskApp />
    </Provider>
  );
  expect(component.find('.add-task').length).toEqual(1);
});

test('adds 4 new task', () => {
  store.dispatch(setNewTask('test1'));
  store.dispatch(addTask());
  store.dispatch(setNewTask('test2'));
  store.dispatch(addTask());
  store.dispatch(setNewTask('test3'));
  store.dispatch(addTask());
  store.dispatch(setNewTask('test4'));
  store.dispatch(addTask());

  const component = render(
    <Provider store={store}>
      <TaskApp />
    </Provider>
  );
  expect(component.find('.task-item').length).toEqual(4);
});

test('deletes one task from list', () => {
  store.dispatch(deleteTask());

  const component = render(
    <Provider store={store}>
      <TaskApp />
    </Provider>
  );
  expect(component.find('.task-item').length).toEqual(3);
});

test('displays error message', () => {
  store.dispatch(setErrorMsg());

  const component = render(
    <Provider store={store}>
      <TaskApp />
    </Provider>
  );
  expect(component.find('.error-message').length).toEqual(1);
});

test('displays success message', () => {
  store.dispatch(setSuccessMsg());

  const component = render(
    <Provider store={store}>
      <TaskApp />
    </Provider>
  );
  expect(component.find('.success-message').length).toEqual(1);
});

test('edit modal box appears', () => {
  store.dispatch(setEdit());

  const component = render(
    <Provider store={store}>
      <TaskApp />
    </Provider>
  );
  expect(component.find('.btn.edit').length).toEqual(1);
});

test('edit task test1 to apple', () => {
  store.dispatch(setEdit('test1'));
  store.dispatch(setNewTask('apple'));
  store.dispatch(editTask());

  const component = render(
    <Provider store={store}>
      <TaskApp />
    </Provider>
  );
  expect(component.find('.item-text').first().text()).toEqual('apple');
});
