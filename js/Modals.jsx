import React from 'react';
import AddTaskModal from './AddTaskModal';
import ValidationModal from './ValidationModal';

const Modals = () => (
  <div>
    <AddTaskModal />
    <ValidationModal />
  </div>
);

export default Modals;
